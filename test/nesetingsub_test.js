const assert = require('assert')
const mongoose = require('mongoose')
const Author = require('../Models/author')

describe('Nesting records', () => {
    beforeEach((done) => {
        mongoose.connection.collections.authors.drop(() =>{
            done()
        })
    })

    it('Creates an author with sub-documents', (done) => {
        let pat = new Author({
            name: 'Patrick Rothfuss',
            books: [{title:'Name of the wind', pages: 400}]
        })

        pat.save().then(() => {
            Author.findOne({name:'Patrick Rothfuss'}).then(record => {
                assert(record.books.length === 1);
                done()
            })
        })
    })

    it('Add a book to an author', (done) => {
        let pat = new Author({
            name: 'Patrick Rothfuss',
            books: [{title:'Name of the wind', pages: 400}]
        })

        pat.save().then(() => {
            Author.findOne({name:'Patrick Rothfuss'}).then(record => {
                record.books.push({title:"wise man's fear", pages: 500})
                record.save().then(() => {
                    Author.findOne({name: 'Patrick Rothfuss'}).then(result => {
                        assert(result.books.length === 2);
                        done()
                    })
                })
            })
        })
    })
})