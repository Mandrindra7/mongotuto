const assert = require('assert')


const MarioChar = require('../Models/marioChar')

describe('Updating records', () => {

    let char
    beforeEach((done) => {
         char = new MarioChar({
            name: 'Mario',
            weight: 50
        })

        char.save().then(() => {
            done()
        })
    })

    it('update one records in database',(done) => {
        MarioChar.findOneAndUpdate({name: 'Mario'}, {name: 'Luigi'}).then(() => {
          MarioChar.findOne({_id: char._id}).then((result) => {
            assert(result.name === 'Luigi');
            done()
          })
            
           
        })
   })

   it('Increment the weight by 1',(done) => {
    MarioChar.findOneAndUpdate({name: 'Mario'}, {$inc: {weight : 1}}).then(() => {
      MarioChar.findOne({name: 'Mario'}).then((record) => {
        assert(record.weight === 51);
        done()
      })
        
       
    })
})


})