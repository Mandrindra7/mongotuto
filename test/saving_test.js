const assert = require('assert')
const MarioChar = require('../Models/marioChar')
const { doesNotMatch } = require('assert')


describe('saving records test', () => {
    it('Save a record to the database',(done) => {
        let char = new MarioChar({
            name :  'Mario'
        })

        char.save().then(() => {
            assert(char.isNew === false)
            done()
        })
    })
})