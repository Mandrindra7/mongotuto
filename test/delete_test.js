const assert = require('assert')


const MarioChar = require('../Models/marioChar')

describe('Deleting records', () => {

    let char
    beforeEach((done) => {
         char = new MarioChar({
            name: 'Mario'
        })

        char.save().then(() => {
            assert(char.isNew === false)
            done()
        })
    })

    it('remove from database',(done) => {
        MarioChar.findOneAndDelete({name: 'Mario'}).then(() => {
          MarioChar.findOne({name: 'Mario'}).then((result) => {
            assert(result === null);
            done()
          })
            
           
        })
   })

})