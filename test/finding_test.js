const assert = require('assert')
const MarioChar = require('../Models/marioChar')

describe('Finding records', () => {

    let char
    beforeEach((done) => {
         char = new MarioChar({
            name: 'Mario'
        })

        char.save().then(() => {
            assert(char.isNew === false)
            done()
        })
    })

    it('Finds one record from database',(done) => {
        MarioChar.findOne({name: 'Mario'}).then((result) => {
           assert(result.name === 'Mario')
           done()
        })
   })

   it('Finds one record by ID from database',(done) => {
    MarioChar.findOne({_id: char._id}).then((result) => {
       assert(result._id.toString() === char._id.toString())
       done()
    })
})
})

